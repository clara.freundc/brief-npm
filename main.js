import chalk from 'chalk';
import validator from 'validator'; 

// test du module Chalk
//console.log(chalk.blue('Hello, Wolrd!'));
/*const log = console.log;
log(chalk.rgb(123, 45, 67).underline('Underlined reddish color'));
log(chalk.hex('#DEADED').bold('Bold gray!'));*/

const emailList = [
    'bla.dada@truc.com',
    'oscour.brr.r@machin.com',
    'lucas.auchard@gmail.comktokritojhgtr',
    'lutin',
];

for (const eachEmail of emailList) {
    const email = validator.isEmail(eachEmail);
    if (email == true) {
        console.log(`Cet email " ${eachEmail} " est valide (${email})`);
    }
    else if (email == false) {
        console.log(`Cet email " ${eachEmail} " est invalide (${email})`);
    }
}
